from django import template

register = template.Library()


dict_time = {'First name':'Ho','Last name':'Ten','email':'Email','Email address':'Dia chi email','Password':'Mat khau','Password confirmation':'Nhap lai mat khau','Old password':'Mat khau cu','New password':'Mat khau moi','New password confirmation':'Nhap lai mat khau moi','Username':'Ten nguoi dung'}
@register.filter
def to_use(value):
	value = str(value)
	result = ''
	for anh,viet in dict_time.items():
		if anh in value:
			result = viet

	return result