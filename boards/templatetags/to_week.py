from django import template

register = template.Library()


dict_time = {'Monday':'Thu 2','Tuesday':'Thu 3','Wednesday':'Thu 4','Thursday':'Thu 5','Friday':'Thu 6','Saturday':'Thu 7','Sunday':'Chu nhat'}
@register.filter
def to_week(value):
	value = str(value)
	result = ''
	for anh,viet in dict_time.items():
		result = value.replace(anh, viet)
		value = result
		
	return result