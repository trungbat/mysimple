from django.contrib.auth.models import User
from django.db.models import Count
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import UpdateView
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.urls import reverse
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Board, Post, Topic
from .forms import NewTopicForm, PostForm

def home(request):
	boards  = Board.objects.all()

	return render(request, 'boards/home.html',{'boards':boards})

def board_topics(request, pk):
	board = get_object_or_404(Board, pk=pk)
	queyset = board.topics.order_by('-last_updated').annotate(replis=Count('posts')-1)
	
	page = request.GET.get('page',1)
	
	paginator = Paginator(queyset, 20)

	try:
		page_obj = paginator.page(page)
	except PageNotAnInteger:
		page_obj = paginator.page(1)
	except EmptyPage:
		page_obj = paginator.page(paginator.num_pages)

	return render(request,'boards/topics.html',{'board':board,'page_obj':page_obj})

@login_required
def new_topic(request,pk):
	board = get_object_or_404(Board, pk=pk)
	
	if request.method == 'POST':
		form = NewTopicForm(request.POST)
		if form.is_valid():
			# print('hop leeeeeeeeeeeeeeeeeeeeeeeeeeee')
			topic = form.save(commit=False)
			topic.board = board 
			topic.starter = request.user 
			topic.save()
			post = Post.objects.create(
				message = form.cleaned_data.get('message'),
				topic=topic,
				created_by = request.user
			)

			return redirect('topic_posts', pk=pk, topic_pk=topic.pk)
		else:
			form = NewTopicForm()

	else:
		form = NewTopicForm()

	return render(request, 'boards/new_topic.html',{'board':board,'form':form})

def topic_posts(request, pk, topic_pk):
	topic = get_object_or_404(Topic, board__pk=pk,pk=topic_pk)
	session_key = 'viewed_topic_{}'.format(topic.pk)
	if not request.session.get(session_key, False):
		topic.views +=1
		topic.save()
		request.session[session_key] = True
	
	queyset = topic.posts.all()
	page = request.GET.get('page',1)
	paginator = Paginator(queyset, 5)
	try:
		page_obj = paginator.page(page)
	except PageNotAnInteger:
		page_obj = paginator.page(1)
	except EmptyPage:
		page_obj = paginator.page(paginator.num_pages)


	return render(request,'boards/topic_posts.html',{'topic':topic,'page_obj':page_obj})

@login_required
def reply_topic(request,pk,topic_pk):
	topic = get_object_or_404(Topic, board__pk=pk,pk=topic_pk)

	if request.method == 'POST':
		form = PostForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.topic = topic 
			post.created_by = request.user 
			post.save()

			topic.last_updated = timezone.now()
			topic.save()

			topic_url = reverse('topic_posts', kwargs={'pk':pk,'topic_pk':topic_pk})
			topic_post_url = '{url}?page={page}#{id}'.format(
				url = topic_url,
				id = post.pk,
				page=topic.get_page_count()
			)

			return redirect(topic_post_url)
	else:
		form = PostForm()

	return render(request,'boards/reply_topic.html',{'form':form,'topic':topic})

@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
	model = Post
	fields = ('message',)
	template_name = 'boards/edit_post.html'
	pk_url_kwarg = 'post_pk'
	context_object_name = 'post'


	def get_queryset(self):
		queryset = super().get_queryset()

		return queryset.filter(created_by=self.request.user)

	def form_valid(self, form):
		post = form.save(commit=False)
		post.updated_by = self.request.user 
		post.updated_at = timezone.now()
		post.save()

		return redirect('topic_posts', pk=post.topic.board.pk, topic_pk=post.topic.pk )