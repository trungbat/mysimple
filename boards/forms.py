from django import forms
from .models import Topic, Post

class NewTopicForm(forms.ModelForm):
	message = forms.CharField(label = 'Noi dung', widget=forms.Textarea(attrs={'rows':5,'placeholder':'Ban dang nghi gi?'}), max_length=4000)

	class Meta:
		model = Topic
		fields = ['subject','message']

	def __init__(self, *args, **kwargs):
		super(NewTopicForm, self).__init__(*args, **kwargs)
		for field in self.fields.values():
			field.error_messages= {'required':'Truong nay la bat buoc'}

class PostForm(forms.ModelForm):
	class Meta:
		model = Post 
		fields = ['message',]