import math
from django.db import models
from django.utils.text import Truncator
from django.contrib.auth.models import User 
from django.utils.html import mark_safe
from markdown import markdown


class Board(models.Model):
	name = models.CharField(verbose_name='Ten', unique=True, max_length=30)
	description = models.CharField(verbose_name='Gioi thieu',max_length=100)

	def __str__(self):
		return self.name

	def get_posts_count(self):
		return Post.objects.filter(topic__board=self).count()

	def get_last_post(self):
		return Post.objects.filter(topic__board=self).order_by('-created_at').first()


class Topic(models.Model):
	subject = models.CharField(max_length=200,verbose_name='Chu de')
	last_updated = models.DateTimeField(auto_now_add=True)
	board = models.ForeignKey(to='Board',related_name='topics',on_delete=models.CASCADE)
	starter = models.ForeignKey(User, on_delete=models.CASCADE, related_name='topics')
	views = models.PositiveIntegerField(default=0)
	
	def __str__(self):
		return self.subject

	def get_page_count(self):
		count = self.posts.count()
		pages = count/2

		return math.ceil(pages)


class Post(models.Model):
	message = models.TextField(max_length=4000, verbose_name='Noi dung')
	topic = models.ForeignKey(to='Topic',verbose_name='Chu de', on_delete=models.CASCADE, related_name='posts')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(null=True)
	created_by = models.ForeignKey(User,on_delete = models.CASCADE, related_name='posts')
	updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+',null=True)

	def __str__(self):
		truncated_message = Truncator(self.message)
		return truncated_message.chars(30)
	
	def get_message_as_markdown(self):
		return mark_safe(markdown(self.message, safe_mode='escape'))

